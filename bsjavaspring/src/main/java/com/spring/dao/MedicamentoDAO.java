package com.spring.dao;
import java.util.List;
import com.spring.entity.Medicamento;
public interface MedicamentoDAO {
	public void insertMedicamento(Medicamento lab);
	public void updateMedicamento(Medicamento lab);
	public void deleteMedicamento(int cod);
	public Medicamento findMedicamento(int cod);
	public List<Medicamento> listMedicamentos();
}
