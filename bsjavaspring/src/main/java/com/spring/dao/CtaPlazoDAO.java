package com.spring.dao;
import java.util.List;
import com.spring.entity.CtaPlazo;
public interface CtaPlazoDAO {
	public void insertCtaPlazo(CtaPlazo lab);
	public void updateCtaPlazo(CtaPlazo lab);
	public void deleteCtaPlazo(int cod);
	public CtaPlazo findCtaPlazo(int cod);
	public List<CtaPlazo> listCtaPlazos();
}
