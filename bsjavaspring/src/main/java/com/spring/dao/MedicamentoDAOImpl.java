package com.spring.dao;
import java.util.List;
import javax.persistence.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.spring.entity.Medicamento;
@Repository
public class MedicamentoDAOImpl implements MedicamentoDAO{
	//objeto para la conex. de BD
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	@Transactional
	public void insertMedicamento(Medicamento lab) {
		try {
			//crear sesion de la conex.
			Session session=sessionFactory.getCurrentSession();
			//insertar
			session.save(lab);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Override
	@Transactional
	public void updateMedicamento(Medicamento lab) {
		try {
			Session session=sessionFactory.getCurrentSession();
			session.update(lab);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	@Override
	@Transactional
	public void deleteMedicamento(int cod) {
		try {
			Session session=sessionFactory.getCurrentSession();
			Medicamento lab=session.get(Medicamento.class, cod);
			session.delete(lab);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Override
	@Transactional(readOnly=true)
	public Medicamento findMedicamento(int cod) {
		Medicamento lab=null;
		try {
			Session session=sessionFactory.getCurrentSession();
			lab=session.get(Medicamento.class, cod);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lab;
	}

	@Override
	@Transactional(readOnly=true)
	public List<Medicamento> listMedicamentos() {
		List<Medicamento> lista=null;
		try {
			Session session=sessionFactory.getCurrentSession();
			String sql="select med from Medicamento as med inner join med.laboratorio as lab";//HQL
			Query query=session.createQuery(sql);
			lista=query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista;
	}

}







