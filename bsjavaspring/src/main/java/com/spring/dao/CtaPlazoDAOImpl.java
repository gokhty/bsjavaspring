package com.spring.dao;
import java.util.List;
import javax.persistence.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.spring.entity.CtaPlazo;
@Repository
public class CtaPlazoDAOImpl implements CtaPlazoDAO{
	//objeto para la conex. de BD
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	@Transactional
	public void insertCtaPlazo(CtaPlazo lab) {
		try {
			//crear sesion de la conex.
			Session session=sessionFactory.getCurrentSession();
			//insertar
			session.save(lab);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Override
	@Transactional
	public void updateCtaPlazo(CtaPlazo lab) {
		try {
			Session session=sessionFactory.getCurrentSession();
			session.update(lab);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	@Override
	@Transactional
	public void deleteCtaPlazo(int cod) {
		try {
			Session session=sessionFactory.getCurrentSession();
			CtaPlazo lab=session.get(CtaPlazo.class, cod);
			session.delete(lab);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Override
	@Transactional(readOnly=true)
	public CtaPlazo findCtaPlazo(int cod) {
		CtaPlazo lab=null;
		try {
			Session session=sessionFactory.getCurrentSession();
			lab=session.get(CtaPlazo.class, cod);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lab;
	}

	@Override
	@Transactional(readOnly=true)
	public List<CtaPlazo> listCtaPlazos() {
		List<CtaPlazo> lista=null;
		try {
			Session session=sessionFactory.getCurrentSession();
			String sql="select med from CtaPlazo as med inner join med.laboratorio as lab";//HQL
			Query query=session.createQuery(sql);
			lista=query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista;
	}

}







