package com.spring.service;
import java.util.List;
import com.spring.entity.Medicamento;
public interface MedicamentoService {
	public void insertMedicamento(Medicamento lab);
	public void updateMedicamento(Medicamento lab);
	public void deleteMedicamento(int cod);
	public Medicamento findMedicamento(int cod);
	public List<Medicamento> listMedicamentos();
}
