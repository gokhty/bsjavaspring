package com.spring.service;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.spring.dao.MedicamentoDAO;
import com.spring.entity.Medicamento;
@Service
public class MedicamentoServiceImpl implements MedicamentoService{
	@Autowired
	private MedicamentoDAO MedicamentoDAO;
	
	@Override
	public void insertMedicamento(Medicamento lab) {
		MedicamentoDAO.insertMedicamento(lab);
	}
	@Override
	public void updateMedicamento(Medicamento lab) {
		MedicamentoDAO.updateMedicamento(lab);
	}
	@Override
	public void deleteMedicamento(int cod) {
		MedicamentoDAO.deleteMedicamento(cod);
	}
	@Override
	public Medicamento findMedicamento(int cod) {
		return MedicamentoDAO.findMedicamento(cod);
	}
	@Override
	public List<Medicamento> listMedicamentos() {
		return MedicamentoDAO.listMedicamentos();
	}
}
