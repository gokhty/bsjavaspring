package com.spring.service;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.spring.dao.CtaPlazoDAO;
import com.spring.entity.CtaPlazo;
@Service
public class CtaPlazoServiceImpl implements CtaPlazoService{
	@Autowired
	private CtaPlazoDAO CtaPlazoDAO;
	
	@Override
	public void insertCtaPlazo(CtaPlazo lab) {
		CtaPlazoDAO.insertCtaPlazo(lab);
	}
	@Override
	public void updateCtaPlazo(CtaPlazo lab) {
		CtaPlazoDAO.updateCtaPlazo(lab);
	}
	@Override
	public void deleteCtaPlazo(int cod) {
		CtaPlazoDAO.deleteCtaPlazo(cod);
	}
	@Override
	public CtaPlazo findCtaPlazo(int cod) {
		return CtaPlazoDAO.findCtaPlazo(cod);
	}
	@Override
	public List<CtaPlazo> listCtaPlazos() {
		return CtaPlazoDAO.listCtaPlazos();
	}
}
