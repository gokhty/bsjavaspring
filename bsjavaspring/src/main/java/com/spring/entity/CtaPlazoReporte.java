package com.spring.entity;

public class CtaPlazoReporte {
	private String cct;
	private String fecha;
	private String aquien;
	private String direccion;
	private String distrito;
	private String atencion;
	private String fecha_inicio;
	private String plazo_dias;
	private String vence_calculado;
	private String impact_calculado;
	private String tea;
	private String textoestsr;

	public String getCct() {
		return cct;
	}
	public void setCct(String cct) {
		this.cct = cct;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getAquien() {
		return aquien;
	}
	public void setAquien(String aquien) {
		this.aquien = aquien;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getDistrito() {
		return distrito;
	}
	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}
	public String getAtencion() {
		return atencion;
	}
	public void setAtencion(String atencion) {
		this.atencion = atencion;
	}
	public String getFecha_inicio() {
		return fecha_inicio;
	}
	public void setFecha_inicio(String fecha_inicio) {
		this.fecha_inicio = fecha_inicio;
	}
	public String getPlazo_dias() {
		return plazo_dias+" d�as";
	}
	public void setPlazo_dias(String plazo_dias) {
		this.plazo_dias = plazo_dias;
	}
	public String getVence_calculado() {
		return vence_calculado;
	}
	public void setVence_calculado(String vence_calculado) {
		this.vence_calculado = vence_calculado;
	}
	
	public String getImpact_calculado() {
		return "S/ "+impact_calculado;
	}
	public void setImpact_calculado(String impact_calculado) {
		this.impact_calculado = impact_calculado;
	}
	public String getTea() {
		return tea + "% TEA";
	}
	public void setTea(String tea) {
		this.tea = tea;
	}
	public String getTextoestsr() {
		return "De acuerdo a las conversaciones sostenidas con ustedes, estamos renovando el dep�sito a plazo fijo que vence "+textoestsr+" seg�n detalle:";
	}
	public void setTextoestsr(String textoestsr) {
		this.textoestsr = textoestsr;
	}
	
}
