package com.spring.entity;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="tb_medicamento")
public class Medicamento implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="cod_med")
	private int codMedicamento;
	@Column(name="nom_med")
	private String nomMedicamento;
	@Column(name="stock_med")
	private int stockMedicamento;
	@Column(name="pre_med")
	private int preMedicamento;
	@Column(name="fec_ven_med")
	private String fecVenMedicamento;
	
	@ManyToOne
	@JoinColumn(name="cod_lab")
	private Laboratorio laboratorio;
	
	public Medicamento(){
		laboratorio = new Laboratorio();
	}

	public int getCodMedicamento() {
		return codMedicamento;
	}

	public void setCodMedicamento(int codMedicamento) {
		this.codMedicamento = codMedicamento;
	}

	public String getNomMedicamento() {
		return nomMedicamento;
	}

	public void setNomMedicamento(String nomMedicamento) {
		this.nomMedicamento = nomMedicamento;
	}

	public int getStockMedicamento() {
		return stockMedicamento;
	}

	public void setStockMedicamento(int stockMedicamento) {
		this.stockMedicamento = stockMedicamento;
	}

	public int getPreMedicamento() {
		return preMedicamento;
	}

	public void setPreMedicamento(int preMedicamento) {
		this.preMedicamento = preMedicamento;
	}

	public String getFecVenMedicamento() {
		return fecVenMedicamento;
	}

	public void setFecVenMedicamento(String fecVenMedicamento) {
		this.fecVenMedicamento = fecVenMedicamento;
	}

	public Laboratorio getLaboratorio() {
		return laboratorio;
	}

	public void setLaboratorio(Laboratorio laboratorio) {
		this.laboratorio = laboratorio;
	}
	
	
}
