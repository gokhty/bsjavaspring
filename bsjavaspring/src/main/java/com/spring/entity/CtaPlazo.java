package com.spring.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="cta_plazo")
public class CtaPlazo {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
private	int id;
private	int id_banco;
private	String cta_proc;
private double monto;
private int dias_plazo;
private double tea;
private String fec_inicio;
private String fec_cierre;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public int getId_banco() {
	return id_banco;
}
public void setId_banco(int id_banco) {
	this.id_banco = id_banco;
}
public String getCta_proc() {
	return cta_proc;
}
public void setCta_proc(String cta_proc) {
	this.cta_proc = cta_proc;
}
public int getDias_plazo() {
	return dias_plazo;
}
public void setDias_plazo(int dias_plazo) {
	this.dias_plazo = dias_plazo;
}
public double getTea() {
	return tea;
}
public void setTea(double tea) {
	this.tea = tea;
}
public String getFec_inicio() {
	return fec_inicio;
}
public void setFec_inicio(String fec_inicio) {
	this.fec_inicio = fec_inicio;
}
public String getFec_cierre() {
	return fec_cierre;
}
public void setFec_cierre(String fec_cierre) {
	this.fec_cierre = fec_cierre;
}
public double getMonto() {
	return monto;
}
public void setMonto(double monto) {
	this.monto = monto;
}



}
