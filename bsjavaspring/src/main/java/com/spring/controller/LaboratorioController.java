package com.spring.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.spring.entity.Laboratorio;
import com.spring.service.LaboratorioService;
@Controller
public class LaboratorioController {
	
	@Autowired
	private LaboratorioService laboratorioService;
	
	@RequestMapping(value="/registrarLaboratorio")
	public String registrarLaboratorio() {
		try {
			Laboratorio lab=new Laboratorio();
			lab.setDesLaboratorio("Ejemplo");
			laboratorioService.insertLaboratorio(lab);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	@RequestMapping(value="/actualizarLaboratorio")
	public String actualizarLaboratorio() {
		try {
			Laboratorio lab=new Laboratorio();
			lab.setCodLaboratorio(3);
			lab.setDesLaboratorio("prueba 20");
			laboratorioService.updateLaboratorio(lab);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	@RequestMapping(value="/eliminarLaboratorio")
	public String eliminarLaboratorio() {
		try {
			laboratorioService.deleteLaboratorio(9);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	@RequestMapping(value="/buscarLaboratorio")
	public String buscarLaboratorio() {
		try {
			Laboratorio lab;
			lab=laboratorioService.findLaboratorio(4);
			System.out.println(lab.getCodLaboratorio()+"-"+lab.getDesLaboratorio());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	@RequestMapping(value="/listarLaboratorio")
	public String listarLaboratorio() {
		try {
			List<Laboratorio> lista=laboratorioService.listLaboratorios();
			for(Laboratorio lab:lista)
			System.out.println(lab.getCodLaboratorio()+"-"+lab.getDesLaboratorio());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	@RequestMapping(value="/form")
    public String showUserForm(Model model){
        //model.addAttribute("laboratorio", new Laboratorio());
        return "lab";
    }

    @RequestMapping(value="/create", method = RequestMethod.POST)
    public String createUser(
    		@RequestParam String nom
    		){
        System.out.println(nom+"");
        Laboratorio lab=new Laboratorio();
		lab.setDesLaboratorio(nom);
		laboratorioService.insertLaboratorio(lab);
        return "redirect:/form";
    }
	
	
}




