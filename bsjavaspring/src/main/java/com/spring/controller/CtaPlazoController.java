package com.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.spring.entity.CtaPlazo;
import com.spring.service.CtaPlazoService;
@Controller
public class CtaPlazoController {
	
	@Autowired
	private CtaPlazoService ctaPlazoService; 
	
	@RequestMapping(value="/regctaplazo")
    public String showUserForm(Model model){
        //model.addAttribute("laboratorio", new Laboratorio());
        return "RegistraCtaPlazo";
    }
	
	@RequestMapping(value="/ctacreate", method = RequestMethod.POST)
    public String createUser(
    		@RequestParam int id_banco,
    		@RequestParam String cta_proc,
    		@RequestParam double monto,
    		@RequestParam int dias_plazo,
    		@RequestParam double tea,
    		@RequestParam String fec_inicio,
    		@RequestParam String fec_cierre
    		){
		CtaPlazo c = new CtaPlazo();
		c.setId_banco(id_banco);
		c.setCta_proc(cta_proc);
		c.setMonto(monto);
		c.setDias_plazo(dias_plazo);
		c.setTea(tea);
		c.setFec_inicio(fec_inicio);
		c.setFec_cierre(fec_cierre);
		ctaPlazoService.insertCtaPlazo(c);
		//System.out.printf("%s%n%s%n%s%n%s%n%s%n%s%n", id_banco,cta_proc,dias_plazo, tea,fec_inicio,fec_cierre);
        return "redirect:/regctaplazo";
    }

}
