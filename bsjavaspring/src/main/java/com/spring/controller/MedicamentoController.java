package com.spring.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.spring.entity.Medicamento;
import com.spring.service.MedicamentoService;
@Controller
public class MedicamentoController {
	
	@Autowired
	private MedicamentoService MedicamentoService;
	
	@RequestMapping(value="/registrarMedicamento")
	public String registrarMedicamento() {
		try {
			Medicamento lab=new Medicamento();
			lab.setNomMedicamento("Ejemplo");
			MedicamentoService.insertMedicamento(lab);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	@RequestMapping(value="/actualizarMedicamento")
	public String actualizarMedicamento() {
		try {
			Medicamento lab=new Medicamento();
			lab.setCodMedicamento(3);
			lab.setNomMedicamento("prueba 20");
			MedicamentoService.updateMedicamento(lab);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	@RequestMapping(value="/eliminarMedicamento")
	public String eliminarMedicamento() {
		try {
			MedicamentoService.deleteMedicamento(9);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	@RequestMapping(value="/buscarMedicamento")
	public String buscarMedicamento() {
		try {
			Medicamento lab;
			lab=MedicamentoService.findMedicamento(4);
			System.out.println(lab.getCodMedicamento()+"-"+lab.getNomMedicamento());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	@RequestMapping(value="/listarMedicamento")
	public String listarMedicamento() {System.out.println("controlador medicamento join 4");
		try {
			List<Medicamento> lista=MedicamentoService.listMedicamentos();
			for(Medicamento lab:lista)
			System.out.println(lab.getCodMedicamento()+"-"+lab.getNomMedicamento()+" - "+ lab.getLaboratorio().getDesLaboratorio());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	
}




