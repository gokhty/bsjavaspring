<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="http://momentjs.com/downloads/moment.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
<title>Insert title here</title>
</head>
<body  ng-app="goyo" ng-controller="mehizoclick">
<form action="ctacreate" method="POST">
<input  name="id_banco" placeholder="id_banco" ng-model="id_banco"/>
<input  name="cta_proc" placeholder="cta_proc" ng-model="cta_proc"/>
<input  name="monto" placeholder="monto" ng-model="monto"/>
<input  name="dias_plazo" placeholder="dias_plazo" ng-model="dias_plazo"/>
<input  name="tea" placeholder="tea" ng-model="tea"/>
<input  name="fec_inicio" placeholder="fec_inicio" ng-model="fec_inicio"/>
<input  name="fec_cierre" placeholder="fec_cierre" ng-model="fec_cierre"/>
<p ng-click="calcularPlazo()">Calcular</p>
<input type="submit" value="Registrar plazo" />
</form>
 <div class="lsttitulosvalores">
 <div >
 <ul>
	<li>Fecha Vto
	<li>Factor acumulado
	<li>Interes
	<li>Valor final
	<li>Factor periodo colocado
	<li>Intereses periodo inversión
	<li>Valor final del perido inversión
 </ul>
 <ul>
  <li>{{fven}}
 <li>{{factorAcu}}
 <li>{{inter}}
 <li>{{valorf}}
 <li>{{factorp}}
 <li>{{interesp}}
 <li>{{valorfpi}}
 </ul>
 </div>
 </div>
</body>
<script>
var ellanomehacecaso = angular.module('goyo', []);
ellanomehacecaso.controller('mehizoclick', function($scope, $http) {
	
	$scope.calcularPlazo = function(){
		//calcular
		var fven = calcularVencimiento($scope.fec_inicio,$scope.dias_plazo);
		var factorAcu = calcularFactor($scope.tea, $scope.fec_inicio,  $scope.fec_cierre);
		
		var inter = calcularInteres($scope.monto, factorAcu);
		var valorf = calcularValor($scope.monto, inter);
		var factorp = calcularFactor($scope.tea, $scope.fec_inicio, fven);
		var interesp = calcularInteres($scope.monto, factorp);
		var valorfpi = calcularValor($scope.monto, interesp);
		//mostrar
		$scope.fven = fven;
		$scope.factorAcu = factorAcu;
		$scope.inter = inter;
		$scope.valorf = valorf;
		$scope.factorp = factorp;
		$scope.interesp = interesp;
		$scope.valorfpi = valorfpi;
	}
	
	function calcularFactor(tea, fec1, fec2){//return tea + fec1 + fec2;
		var a = parseFloat(1 + (parseFloat(tea)/100));
		var b = parseInt(moment(String(fec2)).diff(moment(String(fec1)), 'days'));
		var c = parseFloat(b/360);
		//return b;
			return Math.pow(a,c)-1;
	}
	function calcularVencimiento(fec, plazo){
		return moment(String(fec)).add(plazo, 'd').format("YYYY-MM-DD");
	}
	function calcularInteres(monto, acumulado){
		return (parseFloat(monto) * parseFloat(acumulado)).toFixed(2);
	}
	function calcularValor(monto, intereses){
		return (parseFloat(monto) + parseFloat(intereses)).toFixed(2);
	}
});
</script>
</html>